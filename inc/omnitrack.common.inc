<?php
/**
 * @file
 * Omnitrack common functions.
 */

/**
 * Returns the request module variables.
 */
function _omnitrack_get_request_variables() {
  $variables = array(
    'sAccount'              => variable_get('omnitrack_s_account', ''),
    'siteID'                => variable_get('omnitrack_siteID', ''),
    'visitorNamespace'      => variable_get('omnitrack_visitor_namespace', ''),
    'dc'                    => variable_get('omnitrack_dc', '122'),
    'trackingServer'        => variable_get('omnitrack_tracking_server', ''),
    'trackingServerSecure'  => variable_get('omnitrack_tracking_server_secure', ''),
    'pageNotFoundTitle'     => variable_get('omnitrack_404_page_name', '404'),
    'isHomePage'            => drupal_is_front_page(),
    'pageTitle'             => _omnitrack_get_page_title(),
    'breadcrumb'            => _omnitrack_get_formatted_breadcrumb(),
    'channel'               => _omnitrack_get_channel(),
    'object'                => _omnitrack_get_current_object(),
  );

  _omnitrack_html_entity_decode_recursive($variables);

  return $variables;
}

/**
 * Performs html decode to UTF8 recursively.
 */
function _omnitrack_html_entity_decode_recursive(&$array) {
  foreach ($array as $key => $item) {
    if (is_string($item)) {
      $array[$key] = html_entity_decode($item, ENT_QUOTES, 'UTF-8');
    }
    elseif (is_array($item)) {
      _omnitrack_html_entity_decode_recursive($item);
      $array[$key] = $item;
    }
  }
}

/**
 * Returns the current page title.
 */
function _omnitrack_get_page_title() {
  $valid_item = menu_get_item();

  $page_title = variable_get('omnitrack_404_page_name', '404');
  if (!empty($valid_item) && $valid_item['access']) {
    $page_title = drupal_get_title();
  }

  return $page_title;
}

/**
 * Returns the breadcrumb of the current page.
 */
function _omnitrack_get_formatted_breadcrumb() {
  $active_breadcrumb = drupal_get_breadcrumb();

  $breadcrumb = array();

  if (!empty($active_breadcrumb)) {
    foreach ($active_breadcrumb as $link) {
      $breadcrumb[] = strip_tags($link);
    }
  }

  $breadcrumb[] = _omnitrack_get_page_title();

  return $breadcrumb;
}

/**
 * Returns a string representing Omniture Channel.
 */
function _omnitrack_get_channel() {
  $channel = NULL;
  $breadcrumb = _omnitrack_get_formatted_breadcrumb();

  if (count($breadcrumb) > 2) {
    array_pop($breadcrumb);
  }

  $channel = end($breadcrumb);

  return $channel;
}

/**
 * Returns the current menu object.
 */
function _omnitrack_get_current_object() {
  $object = menu_get_object();
  if (!empty($object)) {
    $object = array(
      'type' => $object->type,
      'title' => $object->title,
    );
  }

  return $object;
}

/**
 * Sets multiple variables values.
 */
function _omnitrack_define_multiple_values($variables) {
  if (is_array($variables)) {
    foreach ($variables as $name => $value) {
      omnitrack_set_variable($name, $value);
    }
  }
}

/**
 * Adds default variables to JS settings.
 */
function _omnitrack_set_default_js_variables() {
  drupal_add_js(array('omnitrack' => omnitrack_get_variables()), 'setting');
}

/**
 * Include current page js.
 */
function _omnitrack_include_current_page_js() {
  _omnitrack_set_default_js_variables();

  // Default javascript code.
  $default_js_code = variable_get('omnitrack_default_js', '');
  $default_js_code .= _omnitrack_get_current_page_js();
  $default_js_code .= _omnitrack_generate_code_from_active_common_selectors();

  // General selectors code.
  $general_selectors = _omnitrack_generate_code_from_active_behavior_selectors();

  // s_code structure with placeholders.
  $s_code_content = '';
  $s_code_content .= file_get_contents(drupal_get_path('module', 'omnitrack') . '/scripts/s_code.js', 'r');

  $patterns = array(
    '//@CONFIG_SECTION_PLACEHOLDER@',
    '//@CONFIGURABLE_JS_TRACKING_CODE_PLACEHOLDER@',
    '//@GENERAL_SELECTORS_CODE_PLACEHOLDER@',
    '//@GLOBAL_SCOPE_CONFIGURABLE_JS_TRACKING_CODE_PLACEHOLDER@',
    '//@PLUGINS_SECTION_PLACEHOLDER@',
    '//@MODULES_SECTION_PLACEHOLDER@',
    '//@S_CODE_CORE_PLACEHOLDER@',
  );
  $replaces = array(
    variable_get('omnitrack_config_section', ''),
    $default_js_code,
    $general_selectors,
    variable_get('omnitrack_global_scope_js', ''),
    variable_get('omnitrack_plugins_section', ''),
    variable_get('omnitrack_modules_section', ''),
    variable_get('omnitrack_s_code_core', ''),
  );

  $output = str_replace($patterns, $replaces, $s_code_content);

  $options = array(
    'type' => 'inline',
    'scope' => 'footer',
    'weight' => 5000,
  );
  drupal_add_js($output, $options);
}

/**
 * Return current page specific javascript mixing all path match types.
 *
 * Following this precedence:
 *   1. nothing
 *   2. content type definition
 *   3. menu router specification
 *   4. path exact match
 */
function _omnitrack_get_current_page_js() {
  $current_page = _omnitrack_get_path_info();
  if (empty($current_page)) {
    return '';
  }

  // 1. Nothing.
  $omnitrack_source = '';

  // 2. Content type definition.
  $current_object = menu_get_object();
  if (!empty($current_object)) {
    $items = _omnitrack_get_items_by_content_type($current_object->type, 'content_type');
    if (!empty($items)) {
      $omnitrack_source .= $items[0]->source;
    }
  }

  // 3. Menu router specification.
  $items = _omnitrack_get_items_by_path($current_page['path_alias']);
  if (!empty($items)) {
    $omnitrack_source .= $items[0]->source;
  }

  // 4. Path exact match.
  if ($current_page['router'] != $current_page['path_alias'] && $current_page['router'] != $current_page['normal_path']) {
    $items = _omnitrack_get_items_by_path($current_page['router']);
    if (!empty($items)) {
      $omnitrack_source .= $items[0]->source;
    }
  }

  return $omnitrack_source;
}

/**
 * Return all already defined content types in omnitrack table.
 */
function _omnitrack_get_defined_content_types() {
  $content_types = array();

  $query = db_select('omnitrack', 'o')
    ->fields('o')
    ->condition('o.type', 'content_type', '=');
  $result = $query->execute();

  foreach ($result as $row) {
    $content_types[$row->value] = $row->source;
  }

  return !empty($content_types) ? $content_types : FALSE;
}

/**
 * Insert new entry into omnitrack table.
 *
 * @param array $fields
 *   Array containing all column fields.
 */
function _omnitrack_insert_new_entry($fields) {
  try {
    $entry = db_insert('omnitrack')
      ->fields($fields)
      ->execute();

    return $entry;
  }
  catch (Exception $e) {
    drupal_set_message(t('Omnitrack table insertion failed with the message: :msg', array(':msg' => $e->getMessage())), 'error');
  }
}

/**
 * Get path information.
 *
 * @param string $path
 *   String of path to be checked, check current Url if NULL.
 *
 * @return string
 *   if menu is valid, return its:
 *    - normal path (e.g. node/3)
 *    - path alias  (e.g. products/product_name_1
 *    - menu router (e.g. products/%)
 *   empty array otherwise.
 */
function _omnitrack_get_path_info($path = NULL) {
  $path_info = array();

  // If path is empty, get current url.
  if (empty($path)) {
    $path = menu_get_item();
    $path = $path['href'];
  }

  $normal_path = drupal_get_normal_path($path);
  $menu_router_path = _menu_find_router_path($normal_path);

  if (!empty($menu_router_path)) {
    $path_info = array(
      'normal_path' => $normal_path,
      'path_alias'  => drupal_get_path_alias($normal_path),
      'router'      => $menu_router_path,
    );
  }

  return $path_info;
}

/**
 * Load all items for the given $path.
 *
 * Check both normal path and possible aliases.
 *
 * @param string $path
 *   Path to be loaded.
 *
 * @return array
 *   Entries in omnitrack table for the given $path.
 */
function _omnitrack_get_items_by_path($path = NULL) {
  $items = NULL;

  $path_info = _omnitrack_get_path_info($path);

  $result = db_select('omnitrack', 'o')
    ->fields('o')
    ->condition(
      db_or()
      ->condition('o.value', $path_info['normal_path'], '=')
      ->condition('o.value', $path_info['path_alias'], '=')
    )
    ->condition('o.type', 'path', '=')
  ->execute();

  foreach ($result as $row) {
    $items[] = $row;
  }

  return $items;
}

/**
 * Load all items for the given $content_type.
 *
 * @param string $content_type
 *   Content type to be loaded.
 *
 * @return array
 *   Entries in omnitrack table for the given $content_type.
 */
function _omnitrack_get_items_by_content_type($content_type) {
  $items = array();

  if (empty($content_type)) {
    return $items;
  }

  $result = db_select('omnitrack', 'o')
    ->fields('o')
    ->condition('o.value', $content_type, '=')
    ->condition('o.type', 'content_type', '=')
  ->execute();

  foreach ($result as $row) {
    $items[] = $row;
  }

  return $items;
}

/**
 * Returns the whole omnitrack_selectors table ordered by weight.
 *
 * @return object
 *   Object with resulting query.
 */
function omnitrack_selectors_get_all_selectors() {
  return db_select('omnitrack_selectors', 'os')
          ->fields('os')
          ->orderBy('os.weight')
          ->execute();
}

/**
 * Return all active common selectors (i.e., not behavior).
 *
 * @return object
 *   Object with resulting query.
 */
function omnitrack_selectors_get_all_active_common_selectors() {
  return db_select('omnitrack_selectors', 'os')
          ->fields('os')
          ->condition('os.active', 1, '=')
          ->condition('os.is_behavior', 0, '=')
          ->orderBy('os.weight')
          ->execute();
}

/**
 * Generate full common selectors code.
 *
 * @return string
 *   String with full omnitrack drupal behavior code.
 */
function _omnitrack_generate_code_from_active_common_selectors() {
  $selectors = omnitrack_selectors_get_all_active_common_selectors();

  $output = '';
  $output .= "// ------------------------------------------------------[common selectors]\n\n";
  foreach ($selectors as $selector) {
    $output .= _omnitrack_generate_common_selector_code($selector);
  }
  $output .= "// ------------------------------------------------------------------------\n\n";

  return $output;
}

/**
 * Generate one common selector code.
 *
 * @return string
 *   String selector code.
 */
function _omnitrack_generate_common_selector_code($selector) {
  $selector_body_code = _omnitrack_generate_selector_body_code($selector);

  $code = "";
  $code .= "    // $selector->name\n";
  $code .= "    $('$selector->selector').bind('$selector->event', function() {\n";
  $code .= "      $selector_body_code";
  $code .= "    });\n\n";

  return $code;
}

/**
 * Returns all active selectors from omnitrack_selectors table.
 *
 * @return object
 *   Object with resulting query.
 */
function omnitrack_selectors_get_all_active_behavior_selectors() {
  return db_select('omnitrack_selectors', 'os')
          ->fields('os')
          ->condition('os.active', 1, '=')
          ->condition('os.is_behavior', 1, '=')
          ->orderBy('os.weight')
          ->execute();
}

/**
 * Generate full drupal behavior.
 *
 * @return string
 *   String with full omnitrack drupal behavior code.
 */
function _omnitrack_generate_code_from_active_behavior_selectors() {
  $selectors = omnitrack_selectors_get_all_active_behavior_selectors();

  $output = '';
  $output .= "  Drupal.behaviors.omnitrackSelectors = {\n";
  $output .= "    attach: function(context) {\n";
  foreach ($selectors as $selector) {
    $output .= _omnitrack_generate_behavior_selector_code($selector);
  }
  $output .= "    }\n";
  $output .= "  };\n\n";

  return $output;
}

/**
 * Generate one behavior selector code.
 *
 * @return string
 *   String selector code.
 */
function _omnitrack_generate_behavior_selector_code($selector) {
  $selector_specific_class = 'omnitrack-processed' . $selector->osid;

  $selector_body_code = _omnitrack_generate_selector_body_code($selector);

  $code = "";
  $code .= "    // $selector->name\n";
  $code .= "    $('$selector->selector', context)\n";
  $code .= "      .not('.$selector_specific_class').addClass('$selector_specific_class')\n";
  $code .= "      .bind('$selector->event', function() {\n";
  $code .= "      $selector_body_code";
  $code .= "    });\n\n";

  return $code;
}

/**
 * Returs a generated selector body code.
 */
function _omnitrack_generate_selector_body_code($selector) {
  $omnitrack_data_json = _omnitrack_get_selector_omnitrack_data_json($selector);

  $code = "";
  $code .= "      $selector->code_snippet\n";
  $code .= "      var json = $omnitrack_data_json;\n";
  $code .= "      $.each(json.variables, function(index, element) {
                    try {
                      json.variables[index] = eval(element);
                    } catch(e) {
                      // if eval fails, use current value
                    }
                  });\n";

  if ($selector->send_data == 1) {
    $code .= "      omnitrack.sendData(json);\n";
  }
  else {
    $code .= "      omnitrack.setData(json.variables, json.events);\n";
  }

  return $code;
}

/**
 * Return selector's variables values JSON.
 *
 * @return string
 *   String with json.
 */
function _omnitrack_get_selector_omnitrack_data_json($selector) {
  $omnitrack_data = array(
    'variables' => array(
      $selector->variables => $selector->variables_values,
    ),
    'events' => array(
      $selector->events,
    ),
  );

  return drupal_json_encode($omnitrack_data);
}

/**
 * Include files to provide syntax highlight functionality.
 *
 * To hightlight be applied to a textarea it must have 'use-snippet' class.
 */
function _omnitrack_include_code_snippet_syntax_highlight_feature() {
  $module_path = drupal_get_path('module', 'omnitrack');
  drupal_add_js($module_path . '/scripts/plugins/ace/ace.js');
  drupal_add_js($module_path . '/scripts/omnitrack.code_snippet.js');
}
