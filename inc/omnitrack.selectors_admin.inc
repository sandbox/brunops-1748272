<?php
/**
 * @file
 * Omnitrack selectors admin settings
 */

/**
 * Form builder. Configure omniture tracking through jQuery selectors.
 */
function omnitrack_general_selectors_admin_settings($form, &$form_state) {
  module_load_include('inc', 'omnitrack', 'inc/omnitrack.common');
  _omnitrack_include_code_snippet_syntax_highlight_feature();

  $module_path = drupal_get_path('module', 'omnitrack');
  drupal_add_js($module_path . '/scripts/omnitrack.general_selectors.js');
  drupal_add_css($module_path . '/theme/omnitrack.general_selectors.css');

  drupal_add_tabledrag('general-selectors-table', 'order', 'sibling', 'general-selectors-weight');

  $form = array();
  $form['#attributes']['class'][] = 'ace-editors';

  $form['howto'] = array(
    '#markup' => theme('omnitrack_selectors_admin_form_header'),
  );

  $header = array(
    'delete'    => t('Delete'),
    'selectors'  => t('Selectors'),
    'active'    => t('Active'),
    'weight'    => t('Weight'),
  );

  $form['general_selectors_table'] = array(
    '#tree' => TRUE,
    '#header' => $header,
    '#rows' => _omnitrack_selectors_get_form_table_rows(),
    '#theme' => 'table',
    '#attributes' => array('id' => 'general-selectors-table'),
    '#empty'  => t('No content available.'),
  );

  $form['#submit'][] = 'omnitrack_general_selectors_admin_settings_submit';

  return system_settings_form($form);
}

/**
 * Return an array of table rows.
 *
 * Where each row contains each form field already rendered as HTML.
 */
function _omnitrack_selectors_get_form_table_rows() {
  module_load_include('inc', 'omnitrack', 'omnitrack.common');
  $selectors = omnitrack_selectors_get_all_selectors();

  $rows = array();
  $weight = 1;
  foreach ($selectors as $selector) {
    $selector->weight = $weight++;
    $rows[] = _omnitrack_selectors_get_row_definition_from_selector($selector);
  }

  $last_selector = _omnitrack_selectors_get_last_row($weight);
  $rows[] = _omnitrack_selectors_get_row_definition_from_selector($last_selector);

  return $rows;
}

/**
 * Return an array containing minimum row definition required by theme_table.
 */
function _omnitrack_selectors_get_row_definition_from_selector($selector) {
  $row = _omnitrack_selectors_get_filled_row($selector);
  $row = _omnitrack_selectors_get_rendered_row_fields($row);

  $row_definition = array(
    'data' => $row,
    'class' => array('draggable'),
  );

  return $row_definition;
}

/**
 * Returns an stdClass containing the basic information for a selector.
 */
function _omnitrack_selectors_get_last_row($weight = 9999) {
  $selector = new stdClass();
  $selector->osid = 0;
  $selector->weight = $weight;
  $selector->name = 'new selector';

  return $selector;
}

/**
 * Returns the filled row of a selector.
 *
 * Fill values of a row for omnitrack_general_selectors_settings using
 * values in $selector. Leave $selector NULL to create an empty (new) row.
 *
 * @param array $selector
 *   Array containing all selector information.
 *
 * @return array
 *   Array with new form row information.
 */
function _omnitrack_selectors_get_filled_row($selector = NULL) {
  if (!isset($selector->osid)) {
    $selector = _omnitrack_selectors_get_last_row();
  }

  $row = array(
    'delete' => array(
      '#name'     => 'delete' . $selector->osid,
      '#type'     => 'checkbox',
      '#checked'  => FALSE,
      '#attributes'   => array('class' => array('delete-checkbox')),
    ),
    'selectors' => array(
      '#title'        => $selector->name,
      '#type'         => 'fieldset',
      '#collapsible'  => TRUE,
      '#collapsed'    => TRUE,
      '#attributes'   => array('class' => array('selectors-fieldset')),
      'name' => array(
        '#title'  => 'Selector name',
        '#name'   => 'name' . $selector->osid,
        '#description'  => t('Selector name for better identification.'),
        '#type'   => 'textfield',
        '#size'   => 100,
        '#value'  => isset($selector->name) ? $selector->name : '',
      ),
      'selector' => array(
        '#title'  => 'jQuery selector',
        '#name'   => 'selector' . $selector->osid,
        '#description'  => t('jQuery selector to identify to which elements the event shall be binded to.'),
        '#type'   => 'textfield',
        '#size'   => 100,
        '#value'  => isset($selector->selector) ? $selector->selector : '',
      ),
      'event' => array(
        '#title'    => 'Trigger on',
        '#name'     => 'event' . $selector->osid,
        '#description'  => t('Which event will trigger omniture tracking.'),
        '#type'     => 'select',
        '#options'  => _omnitrack_get_selector_trigger_options(),
        '#value'    => isset($selector->event) ? $selector->event : '',
      ),
      'code_snippet' => array(
        '#title'        => 'Code snippet',
        '#name'         => 'code_snippet' . $selector->osid,
        '#description'  => t('Code to insert inside the binded event. Process anything needed before sending data to omniture here. This field have access to <strong>$</strong> jQuery variable.<br />All variables defined here are accessible to "Variables values" field.'),
        '#type'         => 'textarea',
        '#value'        => isset($selector->code_snippet) ? $selector->code_snippet : '',
        '#attributes'   => array(
          'class' => array('use-snippet'),
          'style' => 'font-family: monospace;',
          'id'    => 'code-snippet' . $selector->osid,
        ),
      ),
      'variables_values' => array(
        '#title'  => 'Variables values',
        '#name'   => 'variables_values' . $selector->osid,
        '#description'  => t('All variables will be set with this value. omnitureInfo object is accessible here. Variables defined previously in Code Snippet field are accessible here.'),
        '#type'   => 'textfield',
        '#size'   => 100,
        '#value'  => isset($selector->variables_values) ? $selector->variables_values : '',
        '#attributes'   => array(
          'id'    => 'variables-values' . $selector->osid,
          'class' => array('use-snippet'),
        ),
      ),
      'variables' => array(
        '#title'  => 'Variables',
        '#name'   => 'variables' . $selector->osid,
        '#description'  => t('Variables to track when event is triggered. Insert all variables separated by comma, be careful with case sensitive variable names (e.g., e<strong>V</strong>ar, page<strong>N</strong>ame).'),
        '#type'   => 'textfield',
        '#size'   => 100,
        '#value'  => isset($selector->variables) ? $selector->variables : '',
      ),
      'events' => array(
        '#title'  => 'Events',
        '#name'   => 'events' . $selector->osid,
        '#description'  => t('Events to track when trigger event occurs. Insert all events here separated by comma.'),
        '#type'   => 'textfield',
        '#size'   => 100,
        '#value'  => isset($selector->events) ? $selector->events : '',
      ),
      'is_behavior' => array(
        '#title'  => 'Is behavior?',
        '#name'   => 'is_behavior' . $selector->osid,
        '#description'  => t('Whether this rule should create a behavior or be placed inside document.ready().'),
        '#type'     => 'checkbox',
        '#checked'  => isset($selector->is_behavior) ? $selector->is_behavior : TRUE,
      ),
      'send_data' => array(
        '#title'  => 'Send Data?',
        '#name'   => 'send_data' . $selector->osid,
        '#description'  => t('Wheter the variables and events should be send or just set.'),
        '#type'     => 'checkbox',
        '#checked'  => isset($selector->send_data) ? $selector->send_data : TRUE,
      ),
      'updated' => array(
        '#name'           => 'updated' . $selector->osid,
        '#type'           => 'hidden',
        '#default_value'  => 0,
      ),
    ),
    'active' => array(
      '#name'     => 'active' . $selector->osid,
      '#type'     => 'checkbox',
      '#checked'  => isset($selector->active) ? $selector->active : TRUE,
    ),
    'weight' => array(
      '#name'       => 'weight' . $selector->osid,
      '#type'       => 'textfield',
      '#size'       => 10,
      '#value'      => isset($selector->weight) ? $selector->weight : 1,
      '#attributes' => array('class' => array('general-selectors-weight')),
    ),
  );

  return $row;
}

/**
 * Return rendered HTML of $row.
 *
 * @param array $row
 *   Array with form api definition of all fields inside the $row.
 *
 * @return array
 *   Array with fields already rendered.
 */
function _omnitrack_selectors_get_rendered_row_fields($row) {
  foreach ($row as $field => $row_data) {
    if ($row_data['#type'] == 'fieldset') {
      _omnitrack_process_collapsed_fieldset_elements_inside_table($row_data);
    }
    drupal_render($row_data);
    $row[$field] = $row_data['#children'];
  }

  return $row;
}

/**
 * Process collapsed fieldset elements inside table.
 */
function _omnitrack_process_collapsed_fieldset_elements_inside_table(&$element) {
  // Collapsible fieldsets.
  if (!empty($element['#collapsible'])) {
    $element['#attached']['library'][] = array('system', 'drupal.collapse');
    $element['#attributes']['class'][] = 'collapsible';
    if (!empty($element['#collapsed'])) {
      $element['#attributes']['class'][] = 'collapsed';
    }
  }
}

/**
 * Return events that trigger a selector.
 *
 * @return array
 *   Array with events.
 */
function _omnitrack_get_selector_trigger_options() {
  return array(
    'click'      => 'Click',
    'change'     => 'Change',
    'focus'      => 'Focus',
    'blur'       => 'Blur',
    'mouseenter' => 'Mouse enter',
    'mouseleave' => 'Mouse leave',
    'mousedown'  => 'Mouse down',
  );
}

/**
 * Form submit handler of omnitrack_general_selectors_admin_settings.
 */
function omnitrack_general_selectors_admin_settings_submit($form, &$form_state) {
  module_load_include('inc', 'omnitrack', 'omnitrack.common');

  $selectors = omnitrack_selectors_get_all_selectors();
  foreach ($selectors as $selector) {

    if (isset($form_state['input']['delete' . $selector->osid])) {
      $success = _omnitrack_selectors_delete_entry_by_id($selector->osid);

      if ($success) {
        drupal_set_message(t('The configurations for original selector: ":selector" were successfully deleted.', array(':selector' => $selector->selector)));
      }
    }
    elseif ($form_state['input']['updated' . $selector->osid] == 1) {
      _omnitrack_selectors_update_entry_by_id($selector->osid, $form_state);
    }
  }

  if (!empty($form_state['input']['selector0'])) {
    $new_selector = _omnitrack_selectors_get_entry_fields_from_form_state_by_id($form_state);
    _omnitrack_selectors_insert_new_entry($new_selector);
  }
}

/**
 * Get array of values of entry with $osid from $form_state.
 *
 * @param array $form_state
 *   Form submited values.
 *
 * @param int $osid
 *   Entry id.
 *
 * @return string
 *   Entry information array.
 */
function _omnitrack_selectors_get_entry_fields_from_form_state_by_id(&$form_state, $osid = 0) {
  return array(
    'name'             => $form_state['input']['name' . $osid],
    'selector'         => $form_state['input']['selector' . $osid],
    'event'            => $form_state['input']['event' . $osid],
    'code_snippet'     => $form_state['input']['code_snippet' . $osid],
    'variables'        => $form_state['input']['variables' . $osid],
    'variables_values' => $form_state['input']['variables_values' . $osid],
    'events'           => $form_state['input']['events' . $osid],
    'is_behavior'      => isset($form_state['input']['is_behavior' . $osid]) ? $form_state['input']['is_behavior' . $osid] : 0,
    'send_data'        => isset($form_state['input']['send_data' . $osid]) ? $form_state['input']['send_data' . $osid] : 0,
    'active'           => isset($form_state['input']['active' . $osid]) ? $form_state['input']['active' . $osid] : 0,
    'weight'           => $form_state['input']['weight' . $osid],
  );
}

/**
 * Get entry with $osid.
 *
 * @param int $osid
 *   Entry id.
 *
 * @return object
 *   Entry object with all its fields.
 */
function _omnitrack_selectors_get_pattern_by_id($osid) {
  return db_select('omnitrack_selectors', 'os')
    ->fields('os')
    ->condition('osid', $osid, '=')
    ->execute()
    ->fetchAssoc();
}

/**
 * Insert new entry into omnitrack_selectors table.
 *
 * @param array $fields
 *   Array containing all column fields from omnitrack_selectors DB table.
 */
function _omnitrack_selectors_insert_new_entry($fields) {
  try {
    $entry = db_insert('omnitrack_selectors')
      ->fields($fields)
      ->execute();

    return $entry;
  }
  catch (Exception $e) {
    drupal_set_message(t('Omnitrack selector insertion failed with the message: :msg', array(':msg' => $e->getMessage())), 'error');
  }
}

/**
 * Delete an entry from omnitrack_selectors table with osid equals $osid.
 *
 * @param int $osid
 *   int with entry id.
 *
 * @return bool
 *   Whether or not the deletion was successful.
 */
function _omnitrack_selectors_delete_entry_by_id($osid) {
  $success = db_delete('omnitrack_selectors')
    ->condition('osid', $osid)
    ->execute();

  return $success;
}

/**
 * Update an entry from omnitrack_selectors table.
 *
 * @param int $osid
 *   int with entry id
 */
function _omnitrack_selectors_update_entry_by_id($osid, &$form_state) {
  $entry_fields = _omnitrack_selectors_get_entry_fields_from_form_state_by_id($form_state, $osid);
  db_update('omnitrack_selectors')
    ->fields($entry_fields)
    ->condition('osid', $osid, '=')
    ->execute();
}
