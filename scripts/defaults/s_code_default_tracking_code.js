/**
 * ALL THIS CODE IS COMMENTED AND AIMS TO SERVE AS AN EXAMPLE OF HOW OMNITURE TRACKING IS PERFORMED
 * THIS EXAMPLE SHOWS HOW TO SEND DATA ON PAGE LOAD USING omnitrack AND omnitrackInfo OBJECTS
 * UPDATE ALL CODE TO FOLLOW THE TRACKING SPECIFICATION OF _YOUR WEBSITE_
 *
 * REMEMBER TO ___ERASE___ THIS COMMENT AND UNCOMMENT THE CODE UNDERNEATH
 */
/*
  // Page not found
  if (omnitrackInfo.pageTitle == 'Not Found') {
    s.linkTrackVars = 'pageType';
    s.pageType = 'ErrorPage';
    s.pageName = omnitrackInfo.pageNotFoundTitle + ' : ' + window.location.href;
  }

  var currentYear = new Date().getFullYear();
  var currentUserTimezone = new Date().getTimezoneOffset() / -60;

  var onPageLoadVariables = {
    'variables' : {
      'pageName'      : omnitrackInfo.pageTitle,
      'channel'       : omnitrackInfo.channel,
      'prop1,eVar1'   : [omnitrackInfo.siteID, omnitrackInfo.channel].join('/'),
      'prop9,eVar9'   : omnitrackInfo.pageTitle,
      'prop15,eVar15' : s.getTimeParting('h', currentUserTimezone, currentYear),
      'prop16,eVar16' : s.getTimeParting('d', currentUserTimezone, currentYear),
      'prop17,eVar17' : s.getTimeParting('w', currentUserTimezone, currentYear),
      'prop18,eVar18' : omnitrack.getVisitorStatus(),
      'prop23,eVar23' : document.URL.toLowerCase()
    },
    'events' : [
      'event7'
    ]
  };

  // Product Page Views
  if (omnitrackInfo.object != null && omnitrackInfo.object.type == 'product') {
    onPageLoadVariables.events[onPageLoadVariables.events.length] = 'event6';
    onPageLoadVariables.variables.channel = 'Products';
    onPageLoadVariables.variables['prop1,eVar1'] = [omnitrackInfo.siteID, 'Products'].join('/') + '/';
  }

  // HP Page Views
  if (omnitrackInfo.isHomePage) {
    onPageLoadVariables.events[onPageLoadVariables.events.length] = 'event5';
  }

  omnitrack.setData(onPageLoadVariables.variables, onPageLoadVariables.events);
*/
